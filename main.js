$(function () {
    model.init();
    setInterval(model.init, 10000);
});
var model = {
    currentime: moment().format(),
    cur_month_en: moment().format('ll').substr(0, 3),
    cur_date: moment().format().substr(8, 2),
    cur_month: moment().format().substr(5, 2),
    cur_year: moment().format().substr(0, 4),
    cur_week_en: moment().format('llll').substr(0, 3),
    cur_week: '',
    init: function () {
        if (new Date().getHours() > 21 || new Date().getHours() < 6) {
            $("body, #y_title, #m_title, #m_title_en, #d_title, #w_title, #w_title_en, #ch_title, #holiday").addClass('dark');
        }
        else {
            $("body, #y_title, #m_title, #m_title_en, #d_title, #w_title, #w_title_en, #ch_title, #holiday").removeClass('dark');
        }
        model.currentime = moment().format(),
            model.cur_month_en = moment().format('ll').substr(0, 3),
            model.cur_date = moment().format().substr(8, 2),
            model.cur_month = moment().format().substr(5, 2),
            model.cur_year = moment().format().substr(0, 4),
            model.setLocale('zh-tw');
        //        console.log(model.currentime);  

        $("#d_title").text(model.cur_date);
        $("#m_title").text(model.cur_month);
        $("#y_title").text(model.cur_year);
        $("#w_title").text(model.cur_week);
        model.setLocale('en');
        $("#m_title_en").text(model.cur_month_en.toUpperCase());
        $("#w_title_en").text(model.cur_week_en.toUpperCase());

        $("#ch_title").text(lunar_model.getLunarDateStr(new Date()));

        if (moment().format('HH:mm') == '00:00') {
            model.getholiday();
        }

    },
    setLocale: function (lang) {
        moment.locale(lang);
        model.currentime = moment().format();
        model.cur_week = moment().format('dddd');
        model.cur_month_en = moment().format('ll').substr(0, 3);
        model.cur_week_en = moment().format('llll').substr(0, 3);
    },
    getholiday: function () {
        $("#d_title, #w_title, #w_title_en").removeClass('isholiday');
        $("#holiday").text("");
        $.post('controller.php?action=getholiday', function (data, textStatus, xhr) {
            for (var i = 0; i < data.data.length; i++) {
                if (moment().format('YYYY/M/D') == data.data[i].date) {
                    console.log(data.data[i].date);
                    $("#holiday").text(data.data[i].name);
                    if (data.data[i].isHoliday === "是") {
                        $("#d_title, #w_title, #w_title_en").addClass('isholiday');
                    }
                    break;
                }
                else {
                    $("#d_title, #w_title, #w_title_en").removeClass('isholiday');
                    $("#holiday").text("");
                }
            }
        }, "json");
    },
    changeTheme: function (event) {
        if ($("#customSwitch1:checked").val())
            $("body, #y_title, #m_title, #m_title_en, #d_title, #w_title, #w_title_en, #ch_title, #holiday").addClass('dark');
        else
            $("body, #y_title, #m_title, #m_title_en, #d_title, #w_title, #w_title_en, #ch_title, #holiday").removeClass('dark');
    }
}
var lunar_model = {
    lunarInfo: new Array(0x04bd8, 0x04ae0, 0x0a570, 0x054d5, 0x0d260, 0x0d950, 0x16554, 0x056a0, 0x09ad0, 0x055d2, 0x04ae0, 0x0a5b6, 0x0a4d0, 0x0d250, 0x1d255, 0x0b540, 0x0d6a0, 0x0ada2, 0x095b0, 0x14977, 0x04970, 0x0a4b0, 0x0b4b5, 0x06a50, 0x06d40, 0x1ab54, 0x02b60, 0x09570, 0x052f2, 0x04970, 0x06566, 0x0d4a0, 0x0ea50, 0x06e95, 0x05ad0, 0x02b60, 0x186e3, 0x092e0, 0x1c8d7, 0x0c950, 0x0d4a0, 0x1d8a6, 0x0b550, 0x056a0, 0x1a5b4, 0x025d0, 0x092d0, 0x0d2b2, 0x0a950, 0x0b557, 0x06ca0, 0x0b550, 0x15355, 0x04da0, 0x0a5d0, 0x14573, 0x052d0, 0x0a9a8, 0x0e950, 0x06aa0, 0x0aea6, 0x0ab50, 0x04b60, 0x0aae4, 0x0a570, 0x05260, 0x0f263, 0x0d950, 0x05b57, 0x056a0, 0x096d0, 0x04dd5, 0x04ad0, 0x0a4d0, 0x0d4d4, 0x0d250, 0x0d558, 0x0b540, 0x0b5a0, 0x195a6, 0x095b0, 0x049b0, 0x0a974, 0x0a4b0, 0x0b27a, 0x06a50, 0x06d40, 0x0af46, 0x0ab60, 0x09570, 0x04af5, 0x04970, 0x064b0, 0x074a3, 0x0ea50, 0x06b58, 0x055c0, 0x0ab60, 0x096d5, 0x092e0, 0x0c960, 0x0d954, 0x0d4a0, 0x0da50, 0x07552, 0x056a0, 0x0abb7, 0x025d0, 0x092d0, 0x0cab5, 0x0a950, 0x0b4a0, 0x0baa4, 0x0ad50, 0x055d9, 0x04ba0, 0x0a5b0, 0x15176, 0x052b0, 0x0a930, 0x07954, 0x06aa0, 0x0ad50, 0x05b52, 0x04b60, 0x0a6e6, 0x0a4e0, 0x0d260, 0x0ea65, 0x0d530, 0x05aa0, 0x076a3, 0x096d0, 0x04bd7, 0x04ad0, 0x0a4d0, 0x1d0b6, 0x0d250, 0x0d520, 0x0dd45, 0x0b5a0, 0x056d0, 0x055b2, 0x049b0, 0x0a577, 0x0a4b0, 0x0aa50, 0x1b255, 0x06d20, 0x0ada0),
    numString: "十一二三四五六七八九十",
    lMString: "正二三四五六七八九十冬臘",
    getLunarDateStr: function (curdate) { //取農曆年月日
        var tY = curdate.getFullYear();
        var tM = curdate.getMonth();
        var tD = curdate.getDate();
        var l = new lunar_model.Lunar(curdate);
        var lM = l.month.toFixed(0);
        var pre = (l.isLeap) ? '閏' : '';
        var mStr = pre + lunar_model.lMString[lM - 1] + '月';
        var lD = l.day.toFixed(0) - 1;
        pre = (lD <= 10) ? '初' : ((lD <= 19) ? '十' : ((lD <= 29) ? '廿' : '三'));
        var dStr = pre + lunar_model.numString[lD % 10];
        return mStr + dStr;
    },
    lYearDays: function (y) { //傳回農曆 y年的總天數
        var i, sum = 348
        for (i = 0x8000; i > 0x8; i >>= 1) sum += (lunar_model.lunarInfo[y - 1900] & i) ? 1 : 0
        return (sum + lunar_model.leapDays(y))
    },
    leapDays: function (y) { //傳回農曆 y年閏月的天數
        if (lunar_model.leapMonth(y))
            return ((lunar_model.lunarInfo[y - 1900] & 0x10000) ? 30 : 29)
        else
            return (0)
    },
    leapMonth: function (y) { //傳回農曆 y年閏哪個月 1-12 , 沒閏傳回 0
        return (lunar_model.lunarInfo[y - 1900] & 0xf)
    },
    monthDays: function (y, m) {
        return ((lunar_model.lunarInfo[y - 1900] & (0x10000 >> m)) ? 30 : 29)
    },
    Lunar: function (objDate) {
        //        console.log(objDate);
        var i, leap = 0, temp = 0
        var baseDate = new Date(1900, 0, 31);
        var offset = (objDate - baseDate) / 86400000;

        //        console.log(offset);

        this.dayCyl = offset + 40
        this.monCyl = 14

        for (i = 1900; i < 2050 && offset > 0; i++) {
            temp = lunar_model.lYearDays(i)
            offset -= temp
            this.monCyl += 12
        }

        if (offset < 0) {
            offset += temp;
            i--;
            this.monCyl -= 12
        }

        this.year = i
        this.yearCyl = i - 1864

        leap = lunar_model.leapMonth(i) //閏哪個月
        this.isLeap = false

        for (i = 1; i < 13 && offset > 0; i++) {
            //閏月
            if (leap > 0 && i == (leap + 1) && this.isLeap == false)
            { --i; this.isLeap = true; temp = leapDays(this.year); }
            else
            { temp = lunar_model.monthDays(this.year, i); }

            //解除閏月
            if (this.isLeap == true && i == (leap + 1)) this.isLeap = false

            offset -= temp
            if (this.isLeap == false) this.monCyl++
        }

        if (offset == 0 && leap > 0 && i == leap + 1)
            if (this.isLeap)
            { this.isLeap = false; }
            else
            { this.isLeap = true; --i; --this.monCyl; }

        if (offset < 0) { offset += temp; --i; --this.monCyl; }

        this.month = i
        this.day = offset + 1
    }
}